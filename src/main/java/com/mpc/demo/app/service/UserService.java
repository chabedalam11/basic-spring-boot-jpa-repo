package com.mpc.demo.app.service;

import com.mpc.demo.app.dto.UserDto;
import com.mpc.demo.app.entity.User;

public interface UserService {
	public User findUserByEmail(String email);
	public User saveUser(UserDto userDto);
}
