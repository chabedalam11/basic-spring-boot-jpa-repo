package com.mpc.demo.app.service;


import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mpc.demo.app.entity.Student;
import com.mpc.demo.app.repo.StudentRepository;




@Service("studentService")
public class StudentServiceImpl implements StudentService{
	private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);
	private static final String TAG ="StudentServiceImpl :: {} ";

	@Autowired
	private StudentRepository studentRepository;
	
	@Override
	public boolean saveStudent(Student student) {
		try {
			studentRepository.save(student);
			return true;
		}catch (Exception e) {
			log.error(TAG,e.getMessage());
		}
		return false;
	}

	@Override
	public List<Student> findAllStudent() {
		List<Student> students = new ArrayList<Student>();
		try {
			students =  studentRepository.findAll();
		}catch (Exception e) {
			log.error(TAG,e.getMessage());
		}
		return students;
	}

	@Override
	public boolean updateStudent(Student student) {
		try {
			Student g =  studentRepository.findByCode(student.getCode());
			student.setEntry_user(g.getEntry_user());
			student.setEntry_date(g.getEntry_date());
			studentRepository.save(student);
			return true;
		}catch (Exception e) {
			log.error(TAG,e.getMessage());
		}
		return false;
	}

	@Override
	public boolean deleteStudent(Student student) {
		try {
			studentRepository.delete(student);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String getMaxCode() {
		return studentRepository.getMaxCode();
	}

	
}
