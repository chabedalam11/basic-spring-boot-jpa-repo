package com.mpc.demo.app.service;

import java.util.List;

import com.mpc.demo.app.entity.Student;


public interface StudentService {
	public boolean saveStudent(Student student);
	public List<Student> findAllStudent();
	public boolean updateStudent(Student student);
	public boolean deleteStudent(Student student);
	public String getMaxCode();
}
