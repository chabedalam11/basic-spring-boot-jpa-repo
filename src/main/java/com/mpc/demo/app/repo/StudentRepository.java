package com.mpc.demo.app.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mpc.demo.app.entity.Student;



@Repository("studentRepository")
public interface StudentRepository extends JpaRepository<Student, String>{
	Student findByCode(String code);
	
	@Query("SELECT coalesce(max(CAST(code AS integer)), 100)  FROM  Student")
	String getMaxCode();
}
