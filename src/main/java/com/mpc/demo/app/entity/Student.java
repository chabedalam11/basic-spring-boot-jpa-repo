package com.mpc.demo.app.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stuent")
public class Student {
	@Id
	@Column(name="code",length=5)
	private String code;
	
	@Column(name="name")
	private String name;
	
	@Column(name="entry_user",length=5)
	private String entry_user;
	
	@Column(name="entry_date")
	private Date entry_date;
	
	@Column(name="upd_user",length=5)
	private String upd_user;
	
	@Column(name="upd_date")
	private Date upd_date;
	
	
	public Student() {
		super();
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEntry_user() {
		return entry_user;
	}


	public void setEntry_user(String entry_user) {
		this.entry_user = entry_user;
	}


	public Date getEntry_date() {
		return entry_date;
	}


	public void setEntry_date(Date entry_date) {
		this.entry_date = entry_date;
	}


	public String getUpd_user() {
		return upd_user;
	}


	public void setUpd_user(String upd_user) {
		this.upd_user = upd_user;
	}


	public Date getUpd_date() {
		return upd_date;
	}


	public void setUpd_date(Date upd_date) {
		this.upd_date = upd_date;
	}
}
