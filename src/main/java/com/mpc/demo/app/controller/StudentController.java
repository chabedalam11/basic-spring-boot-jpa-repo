package com.mpc.demo.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mpc.demo.app.entity.Student;
import com.mpc.demo.app.entity.User;
import com.mpc.demo.app.service.StudentService;
import com.mpc.demo.app.service.UserService;
import com.mpc.demo.app.util.AppUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Controller
public class StudentController {
	private static final Logger log = LoggerFactory.getLogger(StudentController.class);
	private static final String TAG ="StudentController :: {} ";
	
	private static User user;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private AppUtils appUtils;
	
	
	@RequestMapping(value={"/admin/student", "/admin/studentedit"}, method = RequestMethod.GET)
	public ModelAndView findstudentPage(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = userService.findUserByEmail(auth.getName());
		//status pass for insert update and delete message
		modelAndView.addObject("status",  "page");
		passingParam(modelAndView);
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/admin/student", method = RequestMethod.POST)
	public ModelAndView savestudent(@ModelAttribute Student student) {
		ModelAndView modelAndView = new ModelAndView();
		log.info(TAG,"student save param : "+genrateMaxCode());
		student.setCode(genrateMaxCode());
		student.setName(student.getName().toUpperCase());
		student.setEntry_date(appUtils.getSqlDate());
		student.setEntry_user(user.getId()+"");
		if(studentService.saveStudent(student)) {
			modelAndView.addObject("status",  "save");
		}else {
			modelAndView.addObject("status",  "error");
		}
		passingParam(modelAndView);
		return modelAndView;
	}
	
	
	@RequestMapping(value="/admin/studentedit", method=RequestMethod.POST, params="action=Update")
	public ModelAndView updatestudent(@ModelAttribute Student student) {
		ModelAndView modelAndView = new ModelAndView();
		log.info(TAG,"student update param : "+student.getCode());
		student.setName(student.getName().toUpperCase());
		student.setUpd_date(appUtils.getSqlDate());
		student.setUpd_user(user.getId()+"");
		if(studentService.updateStudent(student)) {
			modelAndView.addObject("status",  "update");
		}else {
			modelAndView.addObject("status",  "error");
		}
		passingParam(modelAndView);
		return modelAndView;
	}


	@RequestMapping(value="/admin/studentedit", method=RequestMethod.POST, params="action=Delete")
	public ModelAndView deletestudent(@ModelAttribute Student student) {
		ModelAndView modelAndView = new ModelAndView();
		log.info(TAG,"student delete param : "+student.getCode());
		if(studentService.deleteStudent(student)) {
			modelAndView.addObject("status",  "delete");
		}else {
			modelAndView.addObject("status",  "error");
		}
		passingParam(modelAndView);
		return modelAndView;
	}
	
	
	private void passingParam(ModelAndView modelAndView) {
		modelAndView.addObject("userName",  user.getFirstName()+ " " + user.getLastName());
		modelAndView.addObject("student",  new Student());
		modelAndView.addObject("studentList",  studentService.findAllStudent());
		modelAndView.addObject("mode",  "student");
		modelAndView.setViewName("admin/student");
	}
	
	private String genrateMaxCode() {
		int code = Integer.parseInt(studentService.getMaxCode())+1;
		return Integer.toString(code);
	}
	
}
