package com.mpc.demo.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.mpc.demo.app.dto.UserDto;
import com.mpc.demo.app.entity.User;
import com.mpc.demo.app.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Controller
public class SignUpController {
	private static final Logger log = LoggerFactory.getLogger(SignUpController.class);
	private static final String TAG ="SignUpController :: {} ";
	
	@Autowired
	private UserService userService;
	
	
	@Autowired
	ApplicationEventPublisher eventPublisher;
	
	
	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		UserDto user = new UserDto();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(
			  @ModelAttribute("user") @Valid UserDto userDto, 
			  BindingResult bindingResult,WebRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(userDto.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"This user is already add");
		}
		if (bindingResult.hasErrors()) {
			return new ModelAndView("registration", "user", userDto);
		} else {
			User registered =userService.saveUser(userDto);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new UserDto());
			modelAndView.setViewName("registration");
			log.info(TAG,registered.getEmail()+" is registered successfully");
			
			//send email
			
			/*try {
		        String appUrl = request.getContextPath();
		        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
		    } catch (Exception me) {
		    	me.printStackTrace();
		        return new ModelAndView("emailError", "user", new UserDto());
		    }*/
			
		}
		return modelAndView;
	}
	
	

}
