package com.mpc.demo.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mpc.demo.app.entity.User;
import com.mpc.demo.app.service.UserService;



@Controller
public class LoginController {
	//private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	//private static final String TAG ="LoginController :: {} ";
	
	@Autowired
	private UserService userService;
	
	
	
	@Autowired
	ApplicationEventPublisher eventPublisher;

	@RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		//emailService.sendSimpleMessage("chabedalam11@gmail.com", "test 2 ", "Hello chabed");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/admin/form", method = RequestMethod.GET)
	public ModelAndView form(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		//modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("userName",  user.getFirstName()+ " " + user.getLastName());
		modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
		modelAndView.setViewName("admin/form");
		return modelAndView;
	}
	

}
