package com.mpc.demo.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mpc.demo.app.entity.User;
import com.mpc.demo.app.service.UserService;





@Controller
public class HomeController {
	//private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	//private static final String TAG ="LoginController :: {} ";
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		//modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("userName",  user.getFirstName()+ " " + user.getLastName());
		modelAndView.addObject("user",  "2");
		modelAndView.addObject("post",  "5");
		modelAndView.addObject("mode",  "home");
		modelAndView.setViewName("admin/home");
		return modelAndView;
	}
	
	

}
