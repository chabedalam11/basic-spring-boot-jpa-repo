package com.mpc.demo.app.dto;

import java.sql.Date;




public class TradeDto {
	
	private String code;
	private String name;
	private String entry_user;
	private Date entry_date;
	private String upd_user;
	private Date upd_date;
	private String genCode;
	private String manufCode;
	private String active;
	private String genName;
	private String manufName;
	
	
	public TradeDto() {
		super();
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEntry_user() {
		return entry_user;
	}


	public void setEntry_user(String entry_user) {
		this.entry_user = entry_user;
	}


	public Date getEntry_date() {
		return entry_date;
	}


	public void setEntry_date(Date entry_date) {
		this.entry_date = entry_date;
	}


	public String getUpd_user() {
		return upd_user;
	}


	public void setUpd_user(String upd_user) {
		this.upd_user = upd_user;
	}


	public Date getUpd_date() {
		return upd_date;
	}


	public void setUpd_date(Date upd_date) {
		this.upd_date = upd_date;
	}


	public String getGenCode() {
		return genCode;
	}


	public void setGenCode(String genCode) {
		this.genCode = genCode;
	}


	public String getManufCode() {
		return manufCode;
	}


	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}


	public String getActive() {
		return active;
	}


	public void setActive(String active) {
		this.active = active;
	}


	public String getGenName() {
		return genName;
	}


	public void setGenName(String genName) {
		this.genName = genName;
	}


	public String getManufName() {
		return manufName;
	}


	public void setManufName(String manufName) {
		this.manufName = manufName;
	}
}
